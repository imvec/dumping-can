# Este script permite instalar un servidor LAMP en tu raspberry pi
# Para emplearlo debes seguir los siguientes pasos usando la Terminal:
# Entra en la carpeta en la que has descargado el archivo
# Cambia los permisos con el comando "sudo chmod +x LAMP-installer.sh"
# Ejecútalo con "sudo ./LAMP-installer.sh"
# Sigue las instrucciones de la Terminal
 echo ""
 echo ""
 echo ""
 echo "==================================="
 echo "Instalando los ingredientes de LAMP "
 echo "==================================="
 echo ""
sleep 2
 echo ""
 echo "==== Actualizando apt-get ===="
sleep 2
 echo ""
apt-get update
 echo ""
 echo "==== Instalando Apache ===="
sleep 2
 echo ""
apt install apache2 -y
 echo ""
 echo "==== Accediendo a /var/www/html ===="
sleep 2
 echo ""
cd /var/www/html
 echo ""
 echo "==== Obteniendo IP ===="
sleep 2
 echo ""
hostname -I
sleep 7
 echo ""
 echo "==== Instalando PHP ===="
sleep 2
 echo ""
apt install php -y
 echo ""
 echo "==== Eliminando index.html ===="
sleep 2
 echo ""
rm index.html
 echo ""
 echo "==== Instalando MySQL y paquetes PHP-MySQL ===="
sleep 2
 echo ""
apt install mariadb-server php-mysql -y
 echo ""
 echo "==== Reiniciando Apache ===="
sleep 2
 echo ""
service apache2 restart
 echo ""
 echo "==== Iniciando script de instalacion segura ===="
 echo ""
 echo "==== Introduce tu password y responde SI (Y) a todo ===="
sleep 2
 echo ""
mysql_secure_installation
 echo ""
 echo "==== Instalando phpMyAdmin ===="
 echo "==== Selecciona: =============="
 echo "==== Apache2 =================="
 echo "==== OK ======================="
 echo "==== YES ======================"
 echo "==== OK ======================="
sleep 9
 echo ""
apt install phpmyadmin -y
 echo ""
 echo "==== Activando extensión MySQLi ===="
sleep 2
 echo ""
phpenmod mysqli
 echo ""
 echo "==== Reiniciando Apache ===="
sleep 2
 echo ""
service apache2 restart
 echo ""
 echo "==== Asegurando interfaz phpMyAdmin ===="
sleep 2
 echo ""
ln -s /usr/share/phpmyadmin /var/www/html/phpmyadmin
 echo ""
 echo "==== Cambiando permisos de /var/www/html ===="
sleep 2
 echo ""
ls -lh /var/www/
chown -R pi:www-data /var/www/html/
chmod -R 770 /var/www/html/
ls -lh /var/www/
 echo ""
 echo "==== INSTALACION COMPLETADA ===="
 echo "== Visita https://calafou.org =="

